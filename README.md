# Quiz 1 React Native Lanjutan batch Agustus

Berikut ini adalah petunjuk pengerjaan Quiz React Native Lanjutan:

1. Fork Repository ini. Untuk fork bisa di cek bagian paling Atas sebelah kanan.
2. Jika sudah fork repository , clone project tersebut. Ingat **Bukan repository ini yang di clone**
3. Jika sudah di clone, lakukan `npm install` terlebih dahulu
4. Untuk jawaban no. 1 beri nama file Jawaban1.js sedangkan untuk jawaban no. 2 beri nama file Jawaban2.js

## Soal

1. Rubahlah component berikut ini menjadi sebuah Function Component

```
import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class FComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Jhon Doe'
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                name: 'Asep'
            })
        }, 3000)
    }

    render() {
        return (
            <View>
                <Text>{this.state.name}</Text>
            </View>
        )
    }
}

```

2. Buatlah sebuah Consumer untuk menampilkan data kedalam FlatList dari Provider Context API berikut ini

```
import React, { useState, createContext } from 'react'

export const RootContext = createContext();

const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider>
            .....
        </RootContext.Provider>
    )
}

export default ContextAPI;

```

> **NOTE** Jika sudah selesai, lakukan push ke repository Anda (git add, git commit, dan git push).  Jangan lupa submit link commit Anda ke sanbercode.com.