import React, { useState, createContext } from 'react'
import FComponent from './Jawaban1';

export const RootContext = createContext();

const ContextAPI = () => {

    const [name, setName] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider value={{name, setName}}>
            <FComponent />
        </RootContext.Provider>
    )
}

export default ContextAPI;
