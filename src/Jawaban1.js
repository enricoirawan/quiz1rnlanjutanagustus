import React, { useEffect, useContext } from 'react'
import { Text, View, FlatList } from 'react-native'
import {RootContext} from './Jawaban2';

const FComponent = () => {
    const state = useContext(RootContext);

    // useEffect(() => {
    //     setTimeout(() => {
    //         state.setName('Asep');
    //     }, 3000)
    // })

    return (
        <View>
            <FlatList
            data={state.name}
            keyExtractor={item => item.name}
            renderItem={({item}) => (
                <View>
                    <Text>{item.name}</Text>
                    <Text>{item.position}</Text>
                </View>
            )}
             />
        </View>
    )
}

export default FComponent;